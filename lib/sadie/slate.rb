#
# Sadie/lib/sadie/slate.rb
#   by IceDragon
require 'sadie/logger'                 # Sadie's Logging interface
require 'sadie/internal/bit_tool'      # Bit Toolkit
require 'sadie/sasm'                   # SASM Language
require 'sadie/slate/helper'           # Slate Helper
require 'sadie/slate/interface'        # Slate Interface Classes
require 'sadie/slate/virtual_machine'  # Slate Virtual Machine
require 'sadie/slate/cpu'              # Slate CPU
require 'sadie/slate/interpreter'      # Slate Interpreter
require 'sadie/slate/version'          # Slate Version Number