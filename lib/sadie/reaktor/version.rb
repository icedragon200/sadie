#
# Sadie/lib/sadie/reaktor/version.rb
#   dm 15/08/2013
module Sadie
  module Reaktor
    ### constants
    VERSION = "2.1.0".freeze
  end
end
