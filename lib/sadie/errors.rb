#
# Sadie/lib/errors.rb
#
module Sadie

  class SadieError < Exception
  end

  class ReaktorError < SadieError
  end

end